#include "average.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>

void setUp(void)
{
    /* set stuff up here */
}

void tearDown(void)
{
    /* clean stuff up here */
}

void test_sum_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0, 0), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(1, 0), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_avg(1, 1), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(integer_avg(27,38), integer_avg(38,27), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(4.5, integer_avg(2, 7), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(650, integer_avg(1000, 300), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(22, integer_avg(33, 11), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-1, integer_avg(2147483647, 2147483647), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-5, integer_avg(-10, -1), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(-2147483647, 2147483647), "Error in integer_avg");

	
}